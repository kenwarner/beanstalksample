module.exports = {
	default: [
		'msbuild:build',
		'xunit_runner'
	],
	ci: [
		'msbuild:build',
		'xunit_runner',
		'msbuild:deploy'
	]
};
