module.exports = {
	options: {
		xUnit: '.\\packages\\xunit.runner.console.2.0.0\\tools\\xunit.console.exe',
		silent: true,
		teamcity: true
	},
	files: {
		src: [
			'*.Tests/bin/**/*.Tests.dll'
		]
	}
};
