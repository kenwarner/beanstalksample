module.exports = {
	build: {
		src: ['BeanstalkSample.sln'],
		options: {
			projectConfiguration: 'Debug'
		}
	},
	deploy: {
		src: ['BeanstalkSample/BeanstalkSample.csproj'],
		options: {
			targets: ['Rebuild', 'WebPublish'],
			version: 4.0,
			buildParameters: {
				VisualStudioVersion: '12.0',
				DeployOnBuild: 'true',
				DeleteExistingFiles: 'true',
				PublishProfile: ("env" in process && "DeployPublishProfile" in process.env) ? process.env.DeployPublishProfile : 'QA'
			}
		}
	}
};
