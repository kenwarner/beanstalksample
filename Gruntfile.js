/// <vs AfterBuild='uglify' Clean='build' SolutionOpened='build' />
/*global module:false*/
module.exports = function (grunt) {
		var path = require('path');

	//Load configs
	require('load-grunt-config')(grunt);

	//Load external tasks
	grunt.loadTasks(path.join(__dirname, 'grunt_tasks'));
};
